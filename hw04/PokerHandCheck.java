//CSE 2 HW 04
//Jason Schanck

import java.util.Scanner; //import scanner class

public class PokerHandCheck {
 
  public static void main(String[] args) {
    
    int number1a = (int)(Math.random()*51)+1; //pick a random number from one to fifty two for each card
    //use if statements to convert every number from 1 to 13 in order to determine card number
    int number1b;
    if (number1a > 0 && number1a <=13 ) {
      number1b = (int) (number1a); 
    }
    else if  (number1a > 13 && number1a <=26) {
      number1b = (int) (number1a - 13);
    }
    else if (number1a > 26 && number1a <= 39) {
      number1b = (int) (number1a - 26);
    }
    else {
      number1b = (int) (number1a - 39);
    }
    int number2a = (int)(Math.random()*51)+1; 
    int number2b;
    if (number2a > 0 && number2a <=13 ) {
      number2b = (int) (number2a); 
    }
    else if  (number2a > 13 && number2a <=26) {
      number2b = (int) (number2a - 13);
    }
    else if (number2a >26 && number2a <= 39) {
      number2b = (int) (number2a - 26);
    }
    else {
      number2b = (int) (number2a - 39);
    }
    int number3a = (int)(Math.random()*51)+1; 
    int number3b;
    if (number3a > 0 && number3a <=13 ) {
      number3b = (int) (number3a); 
    }
    else if  (number3a > 13 && number3a <=26) {
      number3b = (int) (number3a - 13);
    }
    else if (number3a >26 && number3a <= 39) {
      number3b = (int) (number3a - 26);
    }
    else {
      number3b = (int) (number3a - 39);
    }
    int number4a = (int)(Math.random()*51)+1;
    int number4b;
    if (number4a > 0 && number4a <=13 ) {
      number4b = (int) (number4a); 
    }
    else if  (number4a > 13 && number4a <=26) {
      number4b = (int) (number4a - 13);
    }
    else if (number4a >26 && number4a <= 39) {
      number4b = (int) (number4a - 26);
    }
    else {
      number4b = (int) (number4a - 39);
    }
    int number5a = (int)(Math.random()*51)+1; 
    int number5b;
    if (number5a > 0 && number5a <=13 ) {
      number5b = (int) (number5a); 
    }
    else if  (number5a > 13 && number5a <=26) {
      number5b = (int) (number5a - 13);
    }
    else if (number5a >26 && number5a <= 39) {
      number5b = (int) (number5a - 26);
    }
    else {
      number5b = (int) (number5a - 39);
    }
    
    
    String Suit1=" "; //declare string variables for the suit for each card
    String Suit2=" ";
    String Suit3=" ";
    String Suit4=" ";
    String Suit5=" ";
    
    //Use if statements to determine the suit of each of the five cards
    if (number1a > 0 && number1a <= 13 ) {
      Suit1 = "Diamonds";
    }
    else if (number1a > 13 && number1a <= 26) {
      Suit1 = "Clubs";
    }
    else if (number1a > 26 && number1a <=39) {
      Suit1 = "Hearts";
    }
    else {
      Suit1 = "Spades";
    }
    
        if (number2a > 0 && number2a <= 13 ) {
      Suit2 = "Diamonds";
    }
    else if (number2a > 13 && number2a <= 26) {
      Suit2 = "Clubs";
    }
    else if (number2a > 26 && number2a <=39) {
      Suit2 = "Hearts";
    }
    else {
      Suit2 = "Spades";
    }
    
        if (number3a > 0 && number3a <= 13 ) {
      Suit3 = "Diamonds";
    }
    else if (number3a > 13 && number3a <= 26) {
      Suit3 = "Clubs";
    }
    else if (number3a > 26 && number3a <=39) {
      Suit3 = "Hearts";
    }
    else {
      Suit3 = "Spades";
    }
    
        if (number4a > 0 && number4a <= 13 ) {
      Suit4 = "Diamonds";
    }
    else if (number4a > 13 && number4a <= 26) {
      Suit4 = "Clubs";
    }
    else if (number4a > 26 && number4a <=39) {
      Suit4 = "Hearts";
    }
    else {
      Suit4 = "Spades";
    }
    
        if (number5a > 0 && number5a <= 13 ) {
      Suit5 = "Diamonds";
    }
    else if (number5a > 13 && number5a <= 26) {
      Suit5 = "Clubs";
    }
    else if (number5a > 26 && number5a <=39) {
      Suit5 = "Hearts";
    }
    else {
      Suit5 = "Spades";
    }
    
    System.out.println("You random cards were: ");
    //Use a series of switch statements to determine the number of each of the five cards
    switch (number1b) {
      case 1:
    System.out.println ("The Ace of " + Suit1 ); //print out the card number and suit for each card
        break;
      case 2:
    System.out.println ("The Two of " + Suit1 );
        break;
      case 3:
    System.out.println ("The Three of " + Suit1 );
        break;
      case 4:
    System.out.println ("The Four of " + Suit1 );
        break;
      case 5:
    System.out.println ("The Five of " + Suit1 );
        break;
      case 6:
    System.out.println ("The Six of " + Suit1 );
        break;
      case 7:
    System.out.println ("The Seven of " + Suit1 );
        break;
      case 8:
    System.out.println ("The Eight of " + Suit1 );
        break;
      case 9:
    System.out.println ("The Nine of " + Suit1 );
        break;
      case 10:
    System.out.println ("The Ten of " + Suit1 );
        break;
      case 11:
    System.out.println ("The Jack of " + Suit1 );
        break;
      case 12:
    System.out.println ("The Queen of " + Suit1 );
        break;
      default:
    System.out.println ("The King of " + Suit1 );
        break;
      }
    
     switch (number2b) {
      case 1:
    System.out.println ("The Ace of " + Suit2 );
        break;
      case 2:
    System.out.println ("The Two of " + Suit2 );
        break;
      case 3:
    System.out.println ("The Three of " + Suit2 );
        break;
      case 4:
    System.out.println ("The Four of " + Suit2 );
        break;
      case 5:
    System.out.println ("The Five of " + Suit2 );
        break;
      case 6:
    System.out.println ("The Six of " + Suit2 );
        break;
      case 7:
    System.out.println ("The Seven of " + Suit2 );
        break;
      case 8:
    System.out.println ("The Eight of " + Suit2 );
        break;
      case 9:
    System.out.println ("The Nine of " + Suit2 );
        break;
      case 10:
    System.out.println ("The Ten of " + Suit2 );
        break;
      case 11:
    System.out.println ("The Jack of " + Suit2 );
        break;
      case 12:
    System.out.println ("The Queen of " + Suit2 );
        break;
      default:
    System.out.println ("The King of " + Suit2 );
        break;
      }
    
     switch (number3b) {
      case 1:
    System.out.println ("The Ace of " + Suit3 );
        break;
      case 2:
    System.out.println ("The Two of " + Suit3 );
        break;
      case 3:
    System.out.println ("The Three of " + Suit3 );
        break;
      case 4:
    System.out.println ("The Four of " + Suit3 );
        break;
      case 5:
    System.out.println ("The Five of " + Suit3 );
        break;
      case 6:
    System.out.println ("The Six of " + Suit3 );
        break;
      case 7:
    System.out.println ("The Seven of " + Suit3 );
        break;
      case 8:
    System.out.println ("The Eight of " + Suit3 );
        break;
      case 9:
    System.out.println ("The Nine of " + Suit3 );
        break;
      case 10:
    System.out.println ("The Ten of " + Suit3 );
        break;
      case 11:
    System.out.println ("The Jack of " + Suit3 );
        break;
      case 12:
    System.out.println ("The Queen of " + Suit3 );
        break;
      default:
    System.out.println ("The King of " + Suit3 );
        break;
      }
    
     switch (number4b) {
      case 1:
    System.out.println ("The Ace of " + Suit4 );
        break;
      case 2:
    System.out.println ("The Two of " + Suit4 );
        break;
      case 3:
    System.out.println ("The Three of " + Suit4 );
        break;
      case 4:
    System.out.println ("The Four of " + Suit4 );
        break;
      case 5:
    System.out.println ("The Five of " + Suit4 );
        break;
      case 6:
    System.out.println ("The Six of " + Suit4 );
        break;
      case 7:
    System.out.println ("The Seven of " + Suit4 );
        break;
      case 8:
    System.out.println ("The Eight of " + Suit4 );
        break;
      case 9:
    System.out.println ("The Nine of " + Suit4 );
        break;
      case 10:
    System.out.println ("The Ten of " + Suit4 );
        break;
      case 11:
    System.out.println ("The Jack of " + Suit4 );
        break;
      case 12:
    System.out.println ("The Queen of " + Suit4 );
        break;
      default:
    System.out.println ("The King of " + Suit4 );
        break;
      }
    
     switch (number5b) {
      case 1:
    System.out.println ("The Ace of " + Suit5 );
        break;
      case 2:
    System.out.println ("The Two of " + Suit5 );
        break;
      case 3:
    System.out.println ("The Three of " + Suit5 );
        break;
      case 4:
    System.out.println ("The Four of " + Suit5 );
        break;
      case 5:
    System.out.println ("The Five of " + Suit5 );
        break;
      case 6:
    System.out.println ("The Six of " + Suit5 );
        break;
      case 7:
    System.out.println ("The Seven of " + Suit5 );
        break;
      case 8:
    System.out.println ("The Eight of " + Suit5 );
        break;
      case 9:
    System.out.println ("The Nine of " + Suit5 );
        break;
      case 10:
    System.out.println ("The Ten of " + Suit5 );
        break;
      case 11:
    System.out.println ("The Jack of " + Suit5 );
        break;
      case 12:
    System.out.println ("The Queen of " + Suit5 );
        break;
      default:
    System.out.println ("The King of " + Suit5 );
        break;
      }
    
    //declare a variable in order to count how many like numbers there are in the hand
    int count = 0;
    
    //Determine how many equal numbers there are in the hand
    if (number1b == number2b) {
      count = count + 1;
    }
    if (number1b == number3b) {
      count = count + 1;
    }
    if (number1b == number4b) {
      count = count + 1;
    }
    if (number1b == number5b) {
      count = count + 1;
    }
    if (number2b == number3b) {
      count = count + 1;
    }
    if (number2b == number4b) {
      count = count + 1;
    }
    if (number2b == number5b) {
      count = count + 1;
    }
    if (number3b == number4b) {
      count = count + 1;
    }
    if (number3b == number5b) {
      count = count + 1;
    }
    if (number4b == number5b) {
      count = count + 1;
    }
    
    //use an if statement to print the apporopriate statement depending on how many like card numbers there are
    if (count == 0) {
      System.out.println ("You have a high card hand!");
    }
        else if (count == 1) {
          System.out.println("You have a pair!");
        }
        else {
          System.out.println("You have three of a kind!");
        }
     
    }
  }