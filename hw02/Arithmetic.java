//CSE2 Homework 2 
//Arithmetic


public class Arithmetic {

	public static void main(String args[]) {
		//Number of pairs of pants
		int numPants = 3;
		// Cost per pair of pants
		double pantsPrice = 34.98;

		//Number of sweatshirts
		int numShirts = 2;
		//Cost per shirt
		double shirtPrice = 24.99;

		//Number of belts
		int numBelts = 1;
		//cost per belt
		double beltCost = 33.99;

		//tax rate
		double paSalesTax = 0.06;

		//compute the total cost of all individual kind of item before tax
		double totCostPants = (numPants * pantsPrice);
		double totCostShirts = (numShirts * shirtPrice);
		double totCostBelts = (numBelts * beltCost);

		//compute the total tax of each individual kind of item
		double totTaxPants1 = (int)((totCostPants * paSalesTax)*100);
		double totTaxPants = totTaxPants1/100;  //make it a number with two digits after the decimal
		double totTaxShirts1 = (int)((totCostShirts * paSalesTax)*100);
		double totTaxShirts = totTaxShirts1/100; //two digits after decimal
		double totTaxBelts1 = (int)((totCostBelts * paSalesTax)*100);
		double totTaxBelts = totTaxBelts1/100; //two digits after decimal

		//compute total cost of purchase before tax
		double totCostPurchase = ((totCostPants + totCostShirts) + totCostBelts);

		//compute total sales tax
		double totSalesTax = ((totTaxPants + totTaxShirts) + totTaxBelts);

		//compute total cost of transaction
		double totCostTransaction = (totSalesTax + totCostPurchase);
		
		//print the computed answers
		System.out.println("The total cost of pants before tax is $" + totCostPants);
		System.out.println("The total cost of sweatshirts before tax is $" + totCostShirts);
		System.out.println("The total cost of belts before tax is $" + totCostBelts);
		System.out.println("The total tax on pants is $" + totTaxPants);
		System.out.println("The total tax on sweatshirts is $" + totTaxShirts);
		System.out.println("The total tax on belts is $" + totTaxBelts);
		System.out.println("The total cost of the purchase before tax is $" + totCostPurchase);
		System.out.println("The total sales tax is $" + totSalesTax);
		System.out.println("The total amount paid for the transaction is $" + totCostTransaction);
		
	}
}
