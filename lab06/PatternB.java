//cse2 lab 06
//Jason Schanck
//3-8-19

import java.util.Scanner;

public class PatternB{
    public static void main (String [] args){

      Scanner myScanner = new Scanner( System.in );
      String junkWord = "";
      int num = 0; //variable that user will enter

      System.out.println("Enter an integer from 1 to 10.");
      boolean iCheck = myScanner.hasNextInt();

      if (iCheck == true) {
        num = myScanner.nextInt();
        if (num < 1 || num > 10 ) {
          iCheck = false; //check if the input is a positive integer between 1 and 10
         }
      }     

      while (iCheck == false) {
        if (num == 0) {
          junkWord = myScanner.next(); //dispose of invalid inputs
        }
        System.out.println("You need input of integer type from 1 to 10!");
        System.out.println("Enter an integer from 1 to 10."); //ask for input again
        iCheck = myScanner.hasNextInt();
        if (iCheck == true) {
          num = myScanner.nextInt(); //assign a new value to num
          if (num < 1 || num > 10 ) {
            iCheck = false; //check if the input is a positive integer between 1 and 10
          }
          else {
            break; //leave the while loop if the number is now valid
          }
        }
      }

      String rowPrint; //declare the variable to store the output on
      for (int numRows = num; numRows >= 1; numRows--) { //determine how many rows will be printed
        rowPrint = "";
        for(int numOnRow = numRows; numOnRow >= 1; numOnRow--) { //use a string to create those rows with the proper order of numbers
          rowPrint = numOnRow + " " + rowPrint;
        }
        System.out.println(rowPrint); //print out the string
      }      
         
    }
}