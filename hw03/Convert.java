// CSE2 Homework 2
//converting metetrs to inches

import java.util.Scanner; //import scanner class

public class Convert {
  
  public static void main(String args[]) {
    Scanner myScanner = new Scanner( System.in ); //declare an instance of the scanner
    System.out.print("Enter the distance in meters:  "); //take user input
    double Meter = myScanner.nextDouble(); //declare a variable for the user input
    
    //convert meters ot inches and multiply and divide by 10000 to achieve 4 digits after the decimal
    double Inch = (int)((Meter * 39.37007) * 10000); 
    double finalInch = Inch / 10000;
   
    System.out.println( + Meter + " meters is " + finalInch + " inches"); //Print the final amount of inches
    
  }
}