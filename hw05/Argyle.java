//cse2 Argyle Extra Credit
//Jason Schanck
//3/18/19
import java.util.Scanner;

public class Argyle {
  public static void main (String[] args) {
    Scanner myScanner = new Scanner( System.in );
    String junkWord = "";
    
    int height = 0; //variable that user will enter
    System.out.println("Enter a positive integer for viewing window height.");
    boolean hCheck = myScanner.hasNextInt();
    if (hCheck == true) {
      height = myScanner.nextInt();
      if (height < 1) {
        hCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (hCheck == false) {
      if (height == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive integer!");
      System.out.println("Enter a positive integer for viewing window height."); //ask for input again
      hCheck = myScanner.hasNextInt();
      if (hCheck == true) {
        height = myScanner.nextInt(); //assign a new value to num
        if (height < 1) {
          hCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }
    
    int width = 0; //variable that user will enter
    System.out.println("Enter a positive integer for viewing window width.");
    boolean wCheck = myScanner.hasNextInt();
    if (wCheck == true) {
      width = myScanner.nextInt();
      if (width < 1) {
        wCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (wCheck == false) {
      if (width == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive integer!");
      System.out.println("Enter a positive integer for viewing window width."); //ask for input again
      wCheck = myScanner.hasNextInt();
      if (wCheck == true) {
        width = myScanner.nextInt(); //assign a new value to num
        if (width < 1) {
          wCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }
    
    int dSize = 0; //variable that user will enter
    System.out.println("Enter a positive integer for diamond width.");
    boolean dCheck = myScanner.hasNextInt();
    if (dCheck == true) {
      dSize = myScanner.nextInt();
      if (dSize < 1) {
        dCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (dCheck == false) {
      if (dSize == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive integer!");
      System.out.println("Enter a positive integer for diamond width."); //ask for input again
      dCheck = myScanner.hasNextInt();
      if (dCheck == true) {
        dSize = myScanner.nextInt(); //assign a new value to num
        if (dSize < 1) {
          dCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }    
    
    int sWidth = 0; //variable that user will enter
    System.out.println("Enter a positive odd integer for the width of the argyle stripe.  It must be no more than half the diamond width.");
    boolean sCheck = myScanner.hasNextInt();
    if (sCheck == true) {
      sWidth = myScanner.nextInt();
      if (sWidth < 1 || sWidth > (.5 * dSize) || sWidth % 2 != 1 ) {
        sCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (sCheck == false) {
      if (sWidth == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive odd integer no more than half the diamond width!");
      System.out.println("Enter a positive odd integer for the width of the argyle stripe.  It must be no more than half the diamond width."); //ask for input again
      sCheck = myScanner.hasNextInt();
      if (sCheck == true) {
        sWidth = myScanner.nextInt(); //assign a new value to num
        if (sWidth < 1) {
          sCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }
   
    System.out.println("Enter a first character for the pattern fill."); //prompt the user to enter the characters
    String temp = myScanner.next();
    char first = temp.charAt(0);
    System.out.println("Enter a Second character for the pattern fill.");
    temp = myScanner.next();
    char second = temp.charAt(0);
    System.out.println("Enter a third character for the stripe fill.");
    temp = myScanner.next();
    char third = temp.charAt(0);    
    
    
    for (int j = 0; j <= height; j++) {  //regulate height parameter
      for ( int i = 0; i <= width;) { //regualte width parameter
        for (int y = 0; y < dSize; y++ ){ //cycle through the diamond
          i = 0;
          for ( int f = 0; f < width; f++) { //regulate width parameter
            for (int x = 0; x < dSize; x++) { //cycle throughout the line
              char nextChar = second;
              if (i >= width) { //break once the width parameter is met
                break;
              }              
              if (y-x <= sWidth/2 && y-x >= -sWidth/2) { //determine when to print the stripe
                nextChar = third;
              }
              else if (dSize - y > x) {
                nextChar = first;
              }
              System.out.print(nextChar); //print out the characters one by one
              i++;
              if (i>=width) {
                break;
              }
            }
            for (int x = 0; x <= dSize; x++) {
              if (i>=width) {
                break;
              }          
              int z = dSize - y;
              if (i >= width) {
                break;
              }              
              char nextChar = first;
              if (z-x <= sWidth/2 && z-x >= -sWidth/2) {
                nextChar = third;
              }
              else if (dSize - z > x) {
                nextChar = second;            
              }
              System.out.print(nextChar);
              i++;
              if (i>=width) {
                break;
              }
            }
          }
          System.out.println(); //start a new line
          j++;
          if (j == height) {
            break;
          }
        }
        if (j == height) {
          break;
        }

        for (int y = dSize; y >= 0; y-- ){
          i = 0;
          for ( int f = 0; f < width; f++) {
            for (int x = 0; x < dSize; x++) {
              if (i >= width) {
                break;
              }              
              char nextChar = second;
              if (y-x <= sWidth/2 && y-x >= -sWidth/2) {
                nextChar = third;
              }
              else if (dSize - y > x + 1) {
                nextChar = first;
              }
              System.out.print(nextChar);
              i++;
              if (i>=width) {
                break;
              }
            }
            for (int x = dSize; x >= 0; x--) {
              if (i>=width) {
                break;
              } 
              if (i >= width) {
                break;
              }                  
              char nextChar = second;
              if (y-x <= sWidth/2 && y-x >= -sWidth/2) {
                nextChar = third;
              }
              else if (dSize - y > x) {
                nextChar = first;               
              }
              System.out.print(nextChar);   
              i++;
              if (i>=width) {
                break;
              }
            }
          }
          System.out.println();
          j++;
          if (j == height) {
            break;
          } 
        }
        if (j == height) {
          break;
        }
      }
   }
    
  }
}



