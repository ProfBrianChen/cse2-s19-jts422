//CSE 2 hw 05
//Jason Schanck

//prompt user for...
//course number
// dep name
// number of times it meets per week
// time it starts
// instructor name
// number of students

import java.util.Scanner;

public class Hw05 {
  
  public static void main (String [] args) {
    Scanner myScanner = new Scanner( System.in );
    String junkWord = "";
    int courseNum = 0;
    String depName = "";
    int timePerWeek = 0;
    String timeStart = "";
    String profName = "";
    int numStudent = 0;
    
    System.out.println("Enter the course number as an integer.");
    boolean cCheck = myScanner.hasNextInt();
    while (cCheck == false) {
      junkWord = myScanner.next(); //dispose of invalid inputs
      System.out.println("You need input of integer type!");
      System.out.println("Enter the course number as an integer."); //ask for input again
      cCheck = myScanner.hasNextInt();
      if (cCheck == true) {
        courseNum = myScanner.nextInt(); //assign a new value to courseNum
        if (courseNum < 1) {
          cCheck = false; //check if the input is a positive integer
        }
        else {
          break;
        }
      }
    }
    if (cCheck == true && courseNum == 0) {
      courseNum = myScanner.nextInt();
    }
    
    System.out.println("Enter the department name as a string.");
    boolean dnCheck = myScanner.hasNext();
    while (dnCheck == false) {
      junkWord = myScanner.next(); //dispose of invalid inputs
      System.out.println("You need input of String type");
      System.out.println("Enter the department name as a string."); //ask for input again
      dnCheck = myScanner.hasNext();
      if (dnCheck == true) {
        depName = myScanner.next(); //assign a new value to courseNum
        break;
      }
    }
    if (dnCheck == true && depName == "") {
      depName = myScanner.next();
    }    
    
    System.out.println("Enter the times per week the course meets as an integer.");
    boolean tpwCheck = myScanner.hasNextInt();
    while (tpwCheck == false) {
      junkWord = myScanner.next(); //dispose of invalid inputs
      System.out.println("You need input of integer type!");
      System.out.println("Enter the times per week the course meets as an integer."); //ask for input again
      tpwCheck = myScanner.hasNextInt();
      if (tpwCheck == true) {
        timePerWeek = myScanner.nextInt(); //assign a new value to timePerWeek
        if (timePerWeek < 1) {
          tpwCheck = false; //check if the input is a positive integer
        }
        else {
          break;
        }
      }
    }
    if (tpwCheck == true && timePerWeek == 0) {
      timePerWeek = myScanner.nextInt();
    }
    
    System.out.println("Enter the time the class starts as a string.");
    boolean tsCheck = myScanner.hasNext();
    while (tsCheck == false) {
      junkWord = myScanner.next(); //dispose of invalid inputs
      System.out.println("You need input of String type");
      System.out.println("Enter the time the class starts as a string."); //ask for input again
      tsCheck = myScanner.hasNext();
      if (tsCheck == true) {
        timeStart = myScanner.next(); //assign a new value to timeStart
        break;
      }
    }
    if (tsCheck == true && timeStart == "") {
      timeStart = myScanner.next();
    }        
    
    System.out.println("Enter the instructor name as a string.");
    boolean pnCheck = myScanner.hasNext();
    while (pnCheck == false) {
      junkWord = myScanner.next(); //dispose of invalid inputs
      System.out.println("You need input of String type");
      System.out.println("Enter the instructor name as a string."); //ask for input again
      pnCheck = myScanner.hasNext();
      if (pnCheck == true) {
        profName = myScanner.next(); //assign a new value to profName
        break;
      }
    }
    if (pnCheck == true && profName == "") {
      profName = myScanner.next();
    }        
    
    System.out.println("Enter the number of students in the class as an integer.");
    boolean nsCheck = myScanner.hasNextInt();
    while (nsCheck == false) {
      junkWord = myScanner.next(); //dispose of invalid inputs
      System.out.println("You need input of integer type");
      System.out.println("Enter the number of students in the class as an integer."); //ask for input again
      nsCheck = myScanner.hasNextInt();
      if (nsCheck == true) {
        numStudent = myScanner.nextInt(); //assign a new value to numStudent
        if (numStudent < 1) {
          nsCheck = false; //check if the input is a positive integer
        }
        else {
          break;
        }
      }
    }
    if (nsCheck == true && numStudent == 0) {
      numStudent = myScanner.nextInt();
    }
  }
}