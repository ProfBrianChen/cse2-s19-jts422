//cse2 lab07
//Jason Schanck

import java.util.Random;
import java.util.Scanner;

public class lab07 {
  
  public static String thesis(String thesisSentence) { //method that prints out the first sentence
    System.out.println(thesisSentence);
    return thesisSentence;
  }
  
  public static String adjective() { //generate a random adjective
    Random  randomGenerator = new Random();     
    int adj = randomGenerator.nextInt(10);    
    String b;
    switch (adj) {
      case 0:
        b = "crazy";
        break;
      case 1:
        b = "wild";
        break;
      case 2:
        b = "nice";
        break;
      case 3:
        b = "lazy";
        break;
      case 4:
        b = "angry";
        break;
      case 5:
        b = "intelligent";
        break;
      case 6:
        b = "foolish";
        break;
      case 7:
        b = "mean";
        break;
      case 8:
        b = "odd";
        break;
      default:
        b = "funny";
        break;   
    }
    return b;
  }

  public static String nounSubject() { //generate a noun for the subject
    Random  randomGenerator = new Random();     
    int nounS = randomGenerator.nextInt(10);    
    String b;
    switch (nounS) {
      case 0:
        b = "dog";
        break;
      case 1:
        b = "fox";
        break;
      case 2:
        b = "bear";
        break;
      case 3:
        b = "lizard";
        break;
      case 4:
        b = "dragon";
        break;
      case 5:
        b = "rock";
        break;
      case 6:
        b = "person";
        break;
      case 7:
        b = "deer";
        break;
      case 8:
        b = "tiger";
        break;
      default:
        b = "rhino";
        break;   
    }
    return b;
  }
  
  public static String verbMeth() { //generate a random verb
    Random  randomGenerator = new Random();     
    int verb = randomGenerator.nextInt(10);     
    String b;
    switch (verb) {
      case 0:
        b = "walked";
        break;
      case 1:
        b = "ran";
        break;
      case 2:
        b = "climbed";
        break;
      case 3:
        b = "jumped";
        break;
      case 4:
        b = "fell";
        break;
      case 5:
        b = "shot";
        break;
      case 6:
        b = "slept";
        break;
      case 7:
        b = "ate";
        break;
      case 8:
        b = "coughed";
        break;
      default:
        b = "sneezed";
        break;   
    }
    return b;
  }

  public static String nounObject() { //generate a random nounObject
    Random  randomGenerator = new Random();     
    int nounO = randomGenerator.nextInt(10);      
    String b;
    switch (nounO) {
      case 0:
        b = "squirrel";
        break;
      case 1:
        b = "robin";
        break;
      case 2:
        b = "fish";
        break;
      case 3:
        b = "jellyfish";
        break;
      case 4:
        b = "shark";
        break;
      case 5:
        b = "whale";
        break;
      case 6:
        b = "squid";
        break;
      case 7:
        b = "mouse";
        break;
      case 8:
        b = "rat";
        break;
      default:
        b = "tree";
        break;   
    }
    return b;
  }  
  
  public static String body(String nounCarry) { //generate a random number of body paragraph sentences
    Random  randomGenerator = new Random();
    int sentenceNumber = randomGenerator.nextInt(10);
    String a = " ";
    while (sentenceNumber > 0) { //comntinue to generate sentences until the random number is met
      String nounS = nounSubject();
      String verb = verbMeth();
      String nounS2 = nounSubject();
      String nounO = nounObject();
      String adjective = adjective();
      int useIt = randomGenerator.nextInt(2);
      if (useIt == 1) { //use it as a replacement for the subjecxt randomly
        a = "It used a " + nounS + " and " + verb + " a " + nounS2 + " at the " + adjective + " " + nounO + "."; 
        System.out.println(a);
      }
      else {
        a = "The " + nounCarry + " used a " + nounS + " and " + verb + " a " + nounS2 + " at the " + adjective + " " + nounO + ".";     
        System.out.println(a);      
      }
      sentenceNumber--;
    }
      return a;    
  }
  
  public static String finalSentence(String nounCarry) { //create a final sentence to conclude the paragraph
    Random  randomGenerator = new Random();
    String verb = verbMeth();
    String nounO = nounObject();
    String a = "That " + nounCarry + " " + verb + " his " + nounO + "!";
    System.out.println(a);
    return a;
  }
  
  public static void finalPrint(String finalSentence, String conclusion, String nounSub) { //print out the final paragraph
    System.out.println();
    System.out.println(finalSentence);
    body(nounSub);
    System.out.println(conclusion);
  }
  
  public static void main (String[] args) { //main method
    Scanner Scan = new Scanner( System.in); 
    int i = 0;
    String nounSub = " ";
    String finalSentence = " ";
    String thesisSentence = " ";
    while (i == 0) {
      String adjective = adjective(); //generate all the words needed for the first sentence
      String adjective2 = adjective();    
      String adjective3 = adjective();        
      nounSub = nounSubject();
      String finalVerb = verbMeth();  
      String nounObj = nounObject();    
      if (adjective == adjective2) { //prevent two equal adjectives in a row
        finalSentence = "The " + adjective + " " + nounSub + " " + finalVerb + " the " + adjective3 + " " + nounObj + "."; //construct the string for the first sentence
        thesis(finalSentence);
      }
      else {
        finalSentence = "The " + adjective + " " + adjective2 + " " + nounSub + " " + finalVerb + " the " + adjective3 + " " + nounObj + ".";
        thesis(finalSentence);
      }
      int j = 0;
      while (j == 0) { //prompt the user if they want a new sentence
        System.out.println("Do you want another sentence?  Type yes or no.");
        String response = Scan.next();
        if (response.equals("yes")) {
          j++;
        }
        else if (response.equals("no")) {
          i++;
          j++;
        }
        else {
          System.out.println("you must enter the word yes or the word no.");
        }
      }
    }
    String conclusion = finalSentence(nounSub); //generate the final sentence by calling a method
    finalPrint(finalSentence, conclusion, nounSub); //call the final method to print out the paragraph
  }
}
