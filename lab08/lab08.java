//cse2 lab08
//Jason Schanck

import java.util.Random;
import java.util.Arrays;
import java.lang.Math; //allows for square root to be used
  
public class lab08 {
  
  public static void main (String [] args) {
    Random randomGenerator = new Random();        
    int random = randomGenerator.nextInt(51); //number from 0 to 50
    random = random + 50; //add 50 so a the array will be from 50 to 100 entries long
    int[] myArray = new int[random];
    System.out.println("The array is " + random + " integers long."); //say how long the array is
    int j = 0;
    for (int i = 0; i < random; i++) { //assign a value to each array entry
      j = randomGenerator.nextInt(100);
      myArray[i] = j;
      System.out.println("Entry " + i + " is equal to " + j);
    }
    int range = getRange(myArray, random); //call a method to calc range
    System.out.println("The range is equal to " + range);  
    double mean = getMean(myArray, random); //calc mean
    System.out.println("The mean is equal to " + mean);     
    double stdDev = getStdDev(myArray, random, mean); //calc standard deviation
    System.out.println("The standard deviation is equal to " + stdDev);        
    shuffle(myArray, random); // call a method to shuffle the array
  }
  
  
  
  
  public static int getRange(int[] myArray, int random) { //calc the range
    Arrays.sort(myArray);
    int range = myArray[random-1] - myArray[0];
    return range;
  }
  public static double getMean(int[] myArray, int random) { //calc the mean
    double total = 0;
    for (int i = 0; i < random; i++) {
      total = myArray[i] + total;
    }
    double mean = total/random;
    return mean;
    
  }
  public static double getStdDev(int[] myArray, int random, double mean) { //calc the standard deviation
    double stdDev = 0;
    for (int i = 0; i<random; i++) {
      double num;
      num = myArray[i] - mean;
      num = num * num;
      stdDev = stdDev + num;
    }
    stdDev = stdDev / (random - 1);
    if (stdDev >= 0) {
      stdDev = Math.sqrt(stdDev);
    }
    else {
      stdDev = Math.sqrt(-stdDev);
    }
    return stdDev;
  }
  public static void shuffle(int[] myArray, int random) { //shuffle
    System.out.println("Organized Array:");
    for (int i = 0; i < random; i++) {
      System.out.println("Entry " + i + " is equal to " + myArray[i]);
    }
    Random randomGenerator = new Random();
    System.out.println();       
    int val2;
    for (int i = 0; i < random; i++) { 
    int randomEntry = randomGenerator.nextInt(random);         
      val2 =  myArray[randomEntry];  
      myArray[i] = val2;
      System.out.println("entry " + i + " was shuffled to = " + myArray[randomEntry] );
    }
    System.out.println();
    System.out.println("Shuffled Array:");    
    for (int i = 0; i < random; i++) {
      System.out.println("Entry " + i + " is equal to " + myArray[i]);
    }    
  }

}