//This program will take input from the user regarding the amount a check is for, the desired tip, and the amount of ways it will be split.
//It will then print the amount each person in the group must pay
import java.util.Scanner; //import scanner class
public class Check{
  //main method required for every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //declare an instance of the scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //take user input
    double checkCost = myScanner.nextDouble(); //declare a variable for the user input
    System.out.print("Enter the percentage of the top you would like to pay as a whole number (in the form xx): "); //take additional user input
    double tipPercent = myScanner.nextDouble(); //declare a variable for the user input
    tipPercent /= 100; //We want to convert the tip into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //take additional user input
    int numPeople = myScanner.nextInt(); //declare a variable for the user input
    double totalCost; //declare a variable for the total cost of the check
    double costPerPerson; //declare a variable for the amount each person will pay
    int dollars, dimes, pennies; //whole dollar amount of cost and dimes and pennies to store digits to the right of the decimal point
    totalCost = checkCost * (1 + tipPercent); //calculate the total cost of the check, including tip
    costPerPerson = totalCost / numPeople;   //calculate the amount each person will pay
    //get the whole amount, dropping decimal fraction
    dollars = (int) costPerPerson;  //convert the cost per person into an integer dollar form
    //get dimes amount EX: (int) 6.73*10 % 10 -> 67 % 10 -> 7
    //where the % (mod) operator returns the remainder after the division:  583%100 -> 83, 27%5 -> 2
    dimes = (int) (costPerPerson *10) %10;
    pennies = (int) (costPerPerson * 100) % 10;
    System.out.println ("Each person in the group owes $" + dollars + '.' + dimes + pennies);  //print the final dollar, dime, and penny cost per person
      
  } //end of main method
} //end of class