//CSE2 lab11
//Jason Schanck
//Insertion Sort

import java.util.Arrays;
public class insertionSort {
  public static void main (String[] args) {
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    int iterBest = insertionSort(myArrayBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
 System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);    
  }
  
  public static int insertionSort(int[] list) {
    System.out.println(Arrays.toString(list));
    int iterations = 0; //count how many comparisons there are
    boolean change = false;
    for (int i = 1; i<list.length; i++) { //increment the start point through the array
      iterations++;
      for(int j = i; j>0; j--) { //check if each term is larger or smaller than the one before it
        if (list[j] < list[j-1]) { //if so, swap the terms
          int temp = list[j];
          list[j] = list[j-1];
          list[j-1] = temp;
          iterations++;  
          change = true;
        }
        else {
          break;
        }
      }
      if (change) {
        System.out.println(Arrays.toString(list)); //print the new array if it was motified
      }
    }
    return iterations;
  }
}