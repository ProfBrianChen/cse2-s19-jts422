// Jason Schanck CSE 002 2/1/19
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;  // seconds in trip 1
      int secsTrip2=3220;  // seconds in trip 2
		  int countsTrip1=1561;  // number of counts in trip 1
		  int countsTrip2=9037; //number of counts in trip 2
      double wheelDiameter=27.0,  //  diameter of the wheel
      PI=3.14159, // value of pi
  	  feetPerMile=5280,  // number of feet in a mile
  	  inchesPerFoot=12,   // number of inches in one foot
  	  secondsPerMinute=60;  // number of seconds in one minute
	    double distanceTrip1, distanceTrip2,totalDistance;  //create three doubles
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles // calculate the distance in trip 1
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // calculate distance in trip 2
	    totalDistance=distanceTrip1+distanceTrip2; // calculate total distance
      System.out.println("Trip 1 was "+distanceTrip1+" miles");  //print the distances and total distances
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class