//Jason Schanck CSE2 lab10
import java.util.Random;

public class lab10 {

  public static int[][] increasingMatrix(int width, int height, boolean format) {
    int[][] matrix;
    if (format == true) { // create a rowmajor matrix
      matrix = new int[height][width];
      int k = 1;
      for(int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          matrix[i][j] = k;
          k++;
        }
      }
    }
    else { //create column major
      matrix = new int[width][height];      
      for (int i = 0; i < width; i++) {
        int k = 1;      
        for (int j = 0; j < height; j++) {
          if (j==0) {
            matrix[i][j] = k+i;
          }
          else {
          matrix[i][j] = k+width+i;
          k=k+width;
          }
        }
      }
    }
    return matrix;
  }
  
  public static void printMatrix(int[][] array, boolean format) {
    if (array == null) { //print the matrix
      System.out.println("The array was empty.");
    }
    else {
      int length = array.length;
      for (int i = 0; i < length; i++) { //choose the member array to print
        System.out.print("Entry " + i + ": ");
        for (int j = 0; j < array[i].length; j++) { //print the member array
          System.out.print(array[i][j] + " ");
        }
        System.out.println();
      }
    }
  }
  
  public static int[][] translate (int[][] matrix, boolean format) {
    int[][] finalMatrix;
    if (format == true) { //if already rowmajor no translation needed
      finalMatrix = new int[0][0];
    }
    else {
      int length = matrix.length;
      int memberLength = matrix[0].length;
      finalMatrix = new int[memberLength][length];
      for (int i = 0; i < memberLength; i++) { 
        for (int j = 0; j<length; j++) {
          finalMatrix[i][j] = matrix[j][i];
        }
      }
    }
    return finalMatrix;    
  }
  
  public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb) {
    int width1 = a.length;
    int width2 = b.length;
    int height1 = a[0].length;
    int height2 = b[0].length;
    int[][] addedMatrix = new int[width1][height1]; //create a final matrix
    int[][] a2 = new int[0][0];
    int[][] b2 = new int[0][0];
    if (width1 != height2 || height1 != width2) { //check to see if the dimensions are equal
      System.out.println("The matrices cannot be added!");
      return null;
    }
    else {
      if (formata == false) {
        a2 = translate(a, formata); //translate if needed
      }
      else {
        a2 = a;
      }
      if (formatb == false) {
        b2 = translate(b, formatb);
      }
      else {
        b2 = b;
      }      
      int k = 2;
      for (int i = 0; i < width1; i++) { //add the matrices
        for (int j = 0; j < height1; j++) {
          addedMatrix[i][j] = k;
          k=k+2;
        }
      }
      return addedMatrix;
    }
  } 
  
  public static void main (String [] args) {
    Random randomGenerator = new Random();
    int width1 = 1 + randomGenerator.nextInt(9); //generate random number from 1 to 10
    int height1 = 1 + randomGenerator.nextInt(9);
    int width2 = 1 + randomGenerator.nextInt(9);
    int height2 = 1 + randomGenerator.nextInt(9);  
    int[][] matrix1 = increasingMatrix(width1, height1, true); //create 3 matrices
    int[][] matrix2 = increasingMatrix(width1, height1, false);    
    int[][] matrix3 = increasingMatrix(width2, height2, true);    
    System.out.println("Matrix a:");  //print the matrices
    printMatrix(matrix1, true);
    System.out.println("Matrix b:");    
    printMatrix(matrix2, false);
    System.out.println("Matrix c:");    
    printMatrix(matrix3, true);    
    int[][] matrix12 = addMatrix(matrix1, true, matrix2, false); //add the matrices
    System.out.println("Matrix a plus b:");    
    printMatrix(matrix12, true);
    int[][] matrix13 = addMatrix(matrix1, true, matrix3, true);    
    System.out.println("Matrix a plus c:");    
    printMatrix(matrix13, true);    
    System.out.println();
  }
}