//CSE 2 lab 04
//Jason Schanck
import java.lang.Math; //import math for the random number generator

public class CardGenerator {
  //main method
  public static void main(String [] args) {
    
    int number1 = (int)(Math.random()*51)+1; //pick a random number from one to fifty two
    //Convert the number from one to sixteen to determine the card number
    int number2;
    if (number1 > 0 && number1 <=13 ) {
      number2 = (int) (number1); 
    }
    else if  (number1 > 13 && number1 <=26) {
      number2 = (int) (number1 - 13);
    }
    else if (number1 > 26 && number1 <= 39) {
      number2 = (int) (number1 - 26);
    }
    else {
      number2 = (int) (number1- 39);
    }
    String Suit=" ";
    //Use if statements to determine the suit
    if (number1 > 0 && number1 <= 13 ) {
      Suit = "Diamonds";
    }
    else if (number1 > 13 && number1 <= 26) {
      Suit = "Clubs";
    }
    else if (number1 > 26 && number1 <=39) {
      Suit = "Hearts";
    }
    else {
      Suit = "Spades";
    }
    
    //Use a switch statement to determine the number of the card
    switch (number2){
      case 1:
      System.out.println ("The card is the Ace of " + Suit ); //print out the card number and suit
      break;
      case 2:
      System.out.println ("The card is the Two of " + Suit );
      break;
      case 3:
      System.out.println ("The card is the Three of " + Suit );
      break;
      case 4:
      System.out.println ("The card is the Four of " + Suit );
      break;
      case 5:
      System.out.println ("The card is the Five of " + Suit );
      break;
      case 6:
      System.out.println ("The card is the Six of " + Suit );
      break;
      case 7:
      System.out.println ("The card is the Seven of " + Suit );
      break;
      case 8:
      System.out.println ("The card is the Eight of " + Suit );
      break;
      case 9:
      System.out.println ("The card is the Nine of " + Suit );
      break;
      case 10:
      System.out.println ("The card is the Ten of " + Suit );
      break;
      case 11:
      System.out.println ("The card is the Jack of " + Suit );
      break;
      case 12:
      System.out.println ("The card is the Queen of " + Suit );
      break;
      default:
      System.out.println ("The card is the King of " + Suit );
      break;
   
      }
    
  }
  
}