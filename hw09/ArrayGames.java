//CSE2 hw9
//Jason Schanck

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class ArrayGames {
  
  public static void main (String[] args) {
    Scanner scan = new Scanner( System.in );
    Random randomGenerator = new Random();    
    boolean check = false;
    int a = 0;
    while (check == false) {
      System.out.println("Would you like to run insert or shorten?"); //determine if the user wants to insert or shorten
      String input = scan.next();
      if (input.equals("insert")) {
        check = true; 
        a = 1;
      }
      else if (input.equals("shorten")) {
        check = true; 
        a = 2;        
      }
      else {
        System.out.println("You must enter the word insert of shorten.");
      }
    }
    if (a == 1) { //a = 1 is if they want insert
      System.out.println("Array 1: ");      
      int[] array1 = generate(); //create two arrays
      System.out.println("Array 2: ");
      int[] array2 = generate();
      int[] finalArray = insert(array1, array2); //call the insert method
      System.out.println("Final Array: ");
      print(finalArray); //print the final array         
    }
    if (a==2) { //a = 2 is i they want shorten
      int[] array = generate(); //create an array and a random number to shorten at
      int number = randomGenerator.nextInt(20);
      int[] finalArray = shorten(array, number); //call the shorten method
      print(finalArray);        
    }
  }
  
  public static int[] generate() {
    Random randomGenerator = new Random(); //create a method with 10 to 20 entries
    int j = randomGenerator.nextInt(10) + 10;
    int[] array = new int[j];
    for (int i = 0; i < j; i++) {
      array[i] = randomGenerator.nextInt(20); //creates an array from 0 to 20
    }
    print(array); //print the array
    return array;
  }
  
  public static void print(int[] array) { //print the inputted array
    int s = array.length;
    System.out.println("Your array is:");
    for (int i = 0; i < s; i++) {
      System.out.println(array[i]); 
    }
  }
  
  public static int[] insert(int[] array1, int[] array2) { //insert the arrays
    Random randomGenerator = new Random();    
    int finalLength = array1.length + array2.length; //find the total length the final array should be
    int inputNum = randomGenerator.nextInt(array1.length); //determine where to insert the second array
    int[] finalArray = new int[finalLength]; //create a new array to combine the other two into
    System.out.println("The second array was inserted before entry " + inputNum + " of the first array.");
    for ( int i = 0; i < inputNum; i++) { //the first entries are from the first array
      finalArray[i] = array1[i];
    }
    int q = 0;
    for (int i = inputNum; i < (inputNum + array2.length); i++ ) { //then insert the entire second array
      finalArray[i] = array2[q];
      q++;
    }
    for (int i = inputNum + array2.length; i < finalLength; i++) { //insert the last of the first array
      finalArray[i] = array1[i-array2.length];
    }
    return finalArray;
  }
  
  public static int[] shorten(int[] array, int number) {
    if (number >= array.length) { //account for if the number is larger than the length of the array
      System.out.println("The entry " + number + " is outside the array.  So the array was not shortened.");      
      return array;
    }
    else {
      int[] finalArray = new int[array.length - 1]; //the final array should be one less than the initial
      for (int i = 0; i < number; i++) {
        finalArray[i] = array[i]; //begin the new array the same as the first array
      }
      for (int i = number; i < finalArray.length; i++) { //account for the deleted entry
        finalArray[i] = array[i+1];
      }
      System.out.println("The array was shortened at entry " + number);
      System.out.println("Shortened array:");
      return finalArray;
    }
  }
  
}