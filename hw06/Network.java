//cse2 homework 6
//Jason Schanck
//3/9/19
import java.util.Scanner;

public class Network {
  public static void main (String[] args) {
    Scanner myScanner = new Scanner( System.in );
    String junkWord = "";
    
    int height = 0; //variable that user will enter
    System.out.println("Enter a positive integer for height.");
    boolean hCheck = myScanner.hasNextInt();
    if (hCheck == true) {
      height = myScanner.nextInt();
      if (height < 1) {
        hCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (hCheck == false) {
      if (height == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive integer!");
      System.out.println("Enter a positive integer for height."); //ask for input again
      hCheck = myScanner.hasNextInt();
      if (hCheck == true) {
        height = myScanner.nextInt(); //assign a new value to num
        if (height < 1) {
          hCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }
    
    int width = 0; //variable that user will enter
    System.out.println("Enter a positive integer for width.");
    boolean wCheck = myScanner.hasNextInt();
    if (wCheck == true) {
      width = myScanner.nextInt();
      if (width < 1) {
        wCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (wCheck == false) {
      if (width == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive integer!");
      System.out.println("Enter a positive integer for width."); //ask for input again
      wCheck = myScanner.hasNextInt();
      if (wCheck == true) {
        width = myScanner.nextInt(); //assign a new value to num
        if (width < 1) {
          wCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }
    
    int sSize = 0; //variable that user will enter
    System.out.println("Enter a positive integer for square size.");
    boolean sCheck = myScanner.hasNextInt();
    if (sCheck == true) {
      sSize = myScanner.nextInt();
      if (sSize < 1) {
        sCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (sCheck == false) {
      if (sSize == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive integer!");
      System.out.println("Enter a positive integer for square size."); //ask for input again
      sCheck = myScanner.hasNextInt();
      if (sCheck == true) {
        sSize = myScanner.nextInt(); //assign a new value to num
        if (sSize < 1) {
          sCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }    
    
    int eLength = 0; //variable that user will enter
    System.out.println("Enter a positive integer for edge length.");
    boolean eCheck = myScanner.hasNextInt();
    if (eCheck == true) {
      eLength = myScanner.nextInt();
      if (eLength < 1) {
        eCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (eCheck == false) {
      if (eLength == 0) {
        junkWord = myScanner.next(); //dispose of invalid inputs
      }
      System.out.println("You need input a positive integer!");
      System.out.println("Enter a positive integer for edge length."); //ask for input again
      eCheck = myScanner.hasNextInt();
      if (eCheck == true) {
        eLength = myScanner.nextInt(); //assign a new value to num
        if (eLength < 1) {
          eCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
    }
    
    String squareTop = ""; //the line with the "#" sign
    String squareTopEdge = ""; //the "||" lines connecting the squares
    String squareSide = ""; //the vertical "|" sides of the squares
    String squareSideEdge = ""; //the horizontal "-" lines connecting the squares
    for(int f = 0; f < width;){ //use this loop to control the total width
      for (int i = 1; i <= sSize; i++) { //create the top and bottom edges of one square
        if (i == 1 || sSize == i) {
          squareTop = squareTop + "#";
          squareSide = squareSide + "|";  
          squareSideEdge = squareSideEdge + "|";
          if (sSize == 1) {  //creating the edges if the square is only size one
              squareTopEdge = squareTopEdge + "|";            
            for (int k = 1; k <= eLength; k++) {
              f++;
              if (f >= width) { //leave the loop once the desired width is reached
                break;
              }              
              squareTop = squareTop + "-";
              squareTopEdge = squareTopEdge + " ";
            }
            if (f >= width) {
              break;
            }
          }
        }
        else {
          squareTop = squareTop + "-"; //form the top and empty body of the squares
          squareSide = squareSide + " ";
          squareSideEdge = squareSideEdge + " ";
        }
        if ((((sSize % 2) == 1 && i == ((sSize/2) + (3/2))) || ((sSize % 2) == 0 && ((i == ((sSize/2) + (3/2))) || (i == ((sSize/2) + (1/2)))))) && (sSize != 1) ) {
            squareTopEdge = squareTopEdge + "|";
        }
        else if (sSize == 1){} //square of size one is built separately
        else {
          squareTopEdge = squareTopEdge + " ";
        }        
        f++;
        if (f >= width) {
          break;
        }     
      }
      if (f >= width) {
        break;
      }
      if ((sSize != 1) && (sSize != 2)) { //size 1 and 2 squares are built separately
        for (int a = 1; a <= eLength; a++) {
          squareSideEdge = squareSideEdge + "-";
          squareSide = squareSide + " ";
          squareTop = squareTop + " ";
          squareTopEdge = squareTopEdge + " ";
          f++;
          if (f >= width) {
            break;
          }
        }
      }
      if (sSize == 2) { //construct size 2 square where the corners also act as the sides
        for (int a = 1; a <= eLength; a++) {
          squareTop = squareTop + "-";
          squareTopEdge = squareTopEdge + " ";
          f++;
          if (f >= width) {
            break;
          }
        }         
      }
    }
    
    for (int c = 0; c < height;){ //print out the lines while keeping in the height parameter
      int j = 1;
      int d = 1;
      for (; j <= sSize; j++) {
        if (j == 1 || sSize == j){
          System.out.println(squareTop); //print the top of the square
          c++;
          if (c >= height){ //leave the loop once the desired height is reached
            break;
          }
        }
        else if ((sSize % 2) == 1 && j == ((sSize/2) + (3/2))) { //print double edges for even squeares
          System.out.println(squareSideEdge);
          c++;
          if (c >= height){
            break;
          }          
        }
        else if ((sSize % 2) == 0 && ((j == ((sSize/2) + (3/2))) || (j==((sSize/2) + (1/2))))) { //print single edges for odd squares
          System.out.println(squareSideEdge);
          c++;
          if (c >= height){
            break;
          }          
        }
        else {
          System.out.println(squareSide); //print the vertical sides of the squares
          c++;
          if (c >= height){
            break;
          }          
        }
      }
      if (c>=height){
        break;
      }
        for (; d <= eLength; d++) { //keep within the height parameter
          System.out.println(squareTopEdge); //print the vertical edges
          c++;
          if (c == height){
            break;
          }          
        }
      if (c>=height){
        break;
      }      
    }
  }
}