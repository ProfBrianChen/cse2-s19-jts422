//cse2 hw7 Area
//Jason Schanck
//3/23/19
import java.util.Scanner;

public class Area {
  
  public static void triangle(Double base, Double height) { //calculate triangle area
    double area = (base * height * .5);
    System.out.println("The area of the triangle is " + area);
  }
  
  public static void rectangle(Double base, Double height) { //calc rectangle area
    double area = (base * height);
    System.out.println("The area of the rectangle is " + area);
  }
  
  public static void circle(Double radius) { //calc circle area
    double area = (radius * radius * 3.14);
    System.out.println("The area of the circle is " + area);
  }  
  
  public static void userInput(String shape) {
    double base = 0;    
    double height = 0;    
    String junkWord = " ";
    double radius = 0;
    Scanner scan = new Scanner( System.in );    
    if (shape.equals("rectangle") || shape.equals("triangle")) {  //triangle and rectangle need base and height dimensions
      while (base == 0) { //loop used to regulate proper user input
        System.out.println("Enter the value for the base as a positive double.");
        boolean bCheck = scan.hasNextDouble();        
        if (bCheck == false) {
          junkWord = scan.next(); //dispose of invalid inputs
          System.out.println("You need input of positive double type!");
          System.out.println("Enter the value of the base as a positive double."); //ask for input again
          bCheck = scan.hasNextDouble();
        }
        if (bCheck == true) {
          base = scan.nextDouble(); //assign a new value to base
          if (base <= 0) {
            bCheck = false; //check if the input is a positive integer
            base = 0;
            System.out.println("You need input of positive double type!");            
          }
          else {
            break;
          }
        }
      }
      
      while (height == 0) { //loop used to regulate proper user input
        System.out.println("Enter the value for the height as a positive double.");
        boolean hCheck = scan.hasNextDouble();        
        if (hCheck == false) {
          junkWord = scan.next(); //dispose of invalid inputs
          System.out.println("You need input of positive double type!");
          System.out.println("Enter the value of the height as a positive double."); //ask for input again
          hCheck = scan.hasNextDouble();
        }
        if (hCheck == true) {
          height = scan.nextDouble(); //assign a new value to height
          if (height <= 0) {
            hCheck = false; //check if the input is a positive integer
            height = 0;
            System.out.println("You need input of positive double type!");            
          }
          else {
            break;
          }
        }
        
      }
      if (shape.equals("rectangle")) {
        rectangle(base, height);
      }
      else {
        triangle (base, height);
      }
    }
    
    else { //circle only needs radius
      while (radius == 0) { //loop used to regulate proper input
        System.out.println("Enter the value for the radius as a positive double.");
        boolean rCheck = scan.hasNextDouble();        
        if (rCheck == false) { //make sure the user enters a positive double
          junkWord = scan.next(); //dispose of invalid inputs
          System.out.println("You need input of positive double type!");
          System.out.println("Enter the value of the radius as a positive double."); //ask for input again
          rCheck = scan.hasNextDouble();
        }
        if (rCheck == true) {
          radius = scan.nextDouble(); //assign a new value to radius
          if (radius <= 0) {
            rCheck = false; //check if the input is a positive integer
            radius = 0;
            System.out.println("You need input of positive double type!");            
          }
          else {
            break;
          }
        }
      } 
      circle(radius);          
    }
  }
  
  public static void main(String[] args) { 
    Scanner scan = new Scanner( System.in );
    System.out.println("Enter the shape you want to calculate the area of.  Enter rectangle, triangle, or circle.");
    String shape = scan.next();
    for (int i = 0; i==0;) {  //ensure the user entered a valid shape
      if (shape.equals("rectangle") || shape.equals("triangle") || shape.equals("circle")){
        userInput(shape); //call the method to ask for demensions
        i++;
      }
      else {
        System.out.println("you must enter the shape you want to calculate the area of.  Enter rectangle, triangle, or circle.");  
        shape = scan.next();      
      }
    }
    
  }
}