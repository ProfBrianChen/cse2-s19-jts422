//cse2 hw7 String Analysis
//Jason Schanck
//3/23/19
import java.util.Scanner;

public class StringAnalysis {
  
  public static void stringCheck(String userString, int examNum) { //method to check the string if user only wants part of it checked
    for (int i = 0; i < examNum && i < userString.length(); i++) { //if the user entered int in larger than string length, analysis stops at the end of the string
      char piece = userString.charAt(i); //pull out a character of the string
      if (Character.isLetter(piece)) { //check if the character is a letter
        System.out.println(piece + " is not a number.");
      }
      else {
        System.out.println(piece + " is a number.");
      }
    }    
  }
  
  public static void stringCheck(String userString) { //method for checking the entire string
    for (int i = 0; i < userString.length(); i++) {
      char piece = userString.charAt(i); //pull out a character of the string
      if (Character.isLetter(piece)) {  //check if the character is a letter
        System.out.println(piece + " is not a number.");
      }
      else {
        System.out.println(piece + " is a number.");
      }
    }
  }
  
  public static void main (String[] args) {
    Scanner scan = new Scanner( System.in );
    System.out.println("Enter a string."); //have the user enter a string
    String userString = scan.next();
    int i = 0;
    while (i < 1) { //regulate proper user input
      System.out.println("Enter the word, all, if you wish to examine all the string characters, or a positive integer if you wish to only examine part of thr string.");
      boolean check = scan.hasNextInt();
      if (check == true) {
        int examNum = scan.nextInt();
        if (examNum < 0) { //make sure a positive int was entered
          System.out.println("Error! negative number entered.");
        }
        else {
          stringCheck(userString, examNum);
          i++;
        }
      }
      else {
        String inputAll = scan.next(); //a string was entered.  Make sure it was the word all and not some other string
        if (inputAll.equals("all")) {
          i++;
          stringCheck(userString);        
        }
        else {
          System.out.println("Incorrect user input.");
        }
      }
    } 
  }
  
}