//CSE2 hw8
//Jason Schanck

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class PlayLottery {
  
  public static void main (String[] args) {
    int[] userNums = new int[5];     
    Scanner myScanner = new Scanner( System.in );
    int userInput = 0; //variable that user will enter    
    for (int i = 0; i < 5; i++) {
      String junkWord = "";
      System.out.println("Enter integer number " + (i+1) + ".  It must be from 0 to 59.");
      boolean Check = myScanner.hasNextInt();
      if (Check == true) {
        userInput = myScanner.nextInt();
        if (userInput < 0 || userInput > 59) {
          Check = false; //check if the input is a positive integer between 1 and 59
         }
        else {
          userNums[i] = userInput;
        }
      }     
      while (Check == false) {
        if (userInput == 0) {
          junkWord = myScanner.next(); //dispose of invalid inputs
        }
        System.out.println("You need an integer from 0 to 59!");
        System.out.println("Enter integer number " + (i+1));
        Check = myScanner.hasNextInt();
        if (Check == true) {
          userInput = myScanner.nextInt(); //assign a new value to userInput
          if (userInput < 0 || userInput > 59) {
            Check = false; //check if the input is a positive integer between 1 and 59
          }
          else {
            userNums[i] = userInput;
            break; //leave the while loop if the number is now valid
          }
        }
      }
    }
    System.out.print("Your numbers: "); //print out the user's numbers
    for (int i = 0; i < 5; i++) {
      System.out.print(userNums[i] + " ");
    }
    System.out.println();
    
    int[] numsWin = numbersPicked(); //call a function to pick numbers
    System.out.print("The winning numbers are: ");
    for (int i = 0; i < 5; i++) { //print out the pickked numbers
      System.out.print(numsWin[i] + " ");
    }
    System.out.println();
    boolean result = userWins(userNums, numsWin); //call a function to compare the two sets of numbers
    if (result == true) { //print out the final result
      System.out.println("You Win!!!!!");
    }
    else {
      System.out.println("You Lose :(");
    }
  }
  
  
  public static int[] numbersPicked() {
    Random ran = new Random();
    int [] pickedNum = new int[5];
    for ( int i = 0; i < 5; i++) { //generate numbers to compare to user array
      int entry = ran.nextInt(60);
      for (int j = i; j > 0; j--) { //prevent repeats
        if (entry == pickedNum[j-1]) {
          entry = ran.nextInt(60);
          j = i;
        }
      }
      pickedNum[i] = entry;
    }
    return pickedNum;
  }
  
  public static boolean userWins(int[] numsPicked, int[] numsWin) {
    boolean result = true;
    int counter = 0; //used to calculate how many matches
    for (int i = 0; i < 5; i++) {
      int val1 = numsPicked[i];
      int val2 = numsWin[i];
      if (val1 == val2) {
        counter++;
      }
    }
    if (counter != 5) { //if there is anything but five matches, the user lost
      result = false;
    }
    return result;
  }
  
  
}