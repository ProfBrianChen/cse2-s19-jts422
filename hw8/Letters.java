//CSE2 hw8 Letters
//Jason Schanck

import java.util.Random;
import java.util.Arrays;

public class Letters {
  
  public static void main (String[] args) {
    Random ran = new Random();
    int length = ran.nextInt(11); //string will be between 10 and 20 characters long
    length += 10;
    char[] myArray = new char[length];      
    String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (int i = 0; i < length; i++) {
      int entry = ran.nextInt(52);
      char entryChar = alphabet.charAt(entry);
      myArray[i] = entryChar;
    }
    char[] myArrayAtoM = getAtoM(myArray, length);
    char[] myArrayNtoZ = getNtoZ(myArray, length); 
    System.out.print("Random character array: ");
    System.out.println(myArray);
    System.out.print("AtoM characters: ");
    System.out.println(myArrayAtoM);
    System.out.print("NtoZ characters: ");    
    System.out.println(myArrayNtoZ);    
  }
  
  public static char[] getAtoM(char[] myArray, int length) {
    String arraySetUp = "";
    int newLength = 0;
    char placeHold = 0;
    for (int i = 0; i < length; i++) {
      if (myArray[i] < 'N' || (myArray[i] > 'Z' && myArray[i] < 'n')) { //determing if this entry in the array is one of the wanted letters
        newLength++;
        placeHold = myArray[i];
        arraySetUp += placeHold; //use a string to stroe the data that will be put into the array
      }
    }  
    char[] myArrayAtoM = new char[newLength];         
    for (int i = 0; i < newLength; i++) {  
      char getChar = arraySetUp.charAt(i); //pull apart the string into an array
      myArrayAtoM[i] = getChar;
    }
    return myArrayAtoM; //return the new array
  }
  
  public static char[] getNtoZ(char[] myArray, int length) {
    String arraySetUp = "";
    int newLength = 0;
    char placeHold = 0;
    for (int i = 0; i < length; i++) {
      if ((myArray[i] >= 'N' && myArray[i] < 'a') || (myArray[i] >= 'n')) {
        newLength++;
        placeHold = myArray[i];
        arraySetUp += placeHold;
      }
    }  
    char[] myArrayNtoZ = new char[newLength];         
    for (int i = 0; i < newLength; i++) {  
      char getChar = arraySetUp.charAt(i);
      myArrayNtoZ[i] = getChar;
    }
    return myArrayNtoZ;
  }  
  

  
}