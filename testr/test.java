import java.util.Arrays;

public class test {
  public static void main( String[] args) {
    int[] myArray = {0, 1, 2, 3, 4};    
    printArray(myArray);
    circle(myArray);
    printArray(myArray);
  /*  int a = 6;
    int i = linSearch(myArray, a);
    System.out.println("Number " + a + " is at entry " + i + ".");  
    selectionSort(myArray);
    System.out.println("Sorted with selection sort: ");
    printArray(myArray);    
    i = binSearch(myArray, a);
    System.out.println("Number " + a + " is at entry " + i + ".");   
    System.out.println();
    
    int[] arrayB = {3, 5, 3, 1 ,8, 120, 7, 3, 65, 32, 87, 23, 6, 2};
    printArray(arrayB);    
    int f = linSearch(arrayB, a);
    System.out.println("Number " + a + " is at entry " + f + ".");  
    insertionSort(arrayB);
    System.out.println("Sorted with insertion sort: ");
    printArray(arrayB);    
    f = binSearch(arrayB, a);
    System.out.println("Number " + a + " is at entry " + f + ".");       
    */
  }
  
  public static void circle(int[] in) {
    int l = in.length;
    int last = in[l-1];
    for(int i = l-1 ; i>0; i--) {
      in[i] = in[i-1];
    }
    in[0] = last;
  }
  
  public static void insertionSort(int[] a) {
    int l = a.length;
    for (int i = 1; i<l; i++) {
      for (int j = i; j>0; j--) {
        if (a[j] < a[j-1]) {
          int temp = a[j];
          a[j] = a[j-1];
          a[j-1] = temp;
        } 
      }
    }
  }
  
  public static void selectionSort(int[] a) {
    int l = a.length;
    for (int i = 0; i<l; i++) {
      for (int k = i+1; k<l; k++) {
        if (a[k] < a[i]) {
          int temp = a[k];
          a[k] = a[i];
          a[i] = temp;
        }
      }
    }
  }
  
  public static int binSearch(int[] array, int searchTerm) {
    int end = array.length;
    int place = 0;
    int start = 1;  
    while (start <= end) {
      int mid = (start + end)/2; //find the middle of the array        
        if (array[mid] == searchTerm) { //return the entry once the number is found
          return mid;
        }
        if (array[mid] > searchTerm) { //adjust the lower bound
          end = mid-1;
        }
        if (array[mid] < searchTerm) { //adjust the upper bound
          start = mid+1;
        }
    }
    return -1;
  }
  
  public static int linSearch(int[] a, int b) {
    int i = a.length;
    for (int j = 0; j<i; j++) {
      if (a[j] == b) {
        return j;
      }
    }
    return -1;
  }
  
  public static void printArray(int[] a) {
    int i = a.length;
    System.out.print("{ ");
    for ( int u: a) {
      System.out.print(u + " ");
    }
    System.out.println("}");
  }
}