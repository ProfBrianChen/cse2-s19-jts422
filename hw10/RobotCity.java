//CSE2 hw10 robot city
//Jason Schanck
import java.util.Random;

public class RobotCity {

  public static void main(String[] args) {
    int[][][] city = buildCity();
    System.out.println("The initial city.");
    display(city);
    Random randomGenerator = new Random();  
    int k = 1 + randomGenerator.nextInt(19); //beteen 1 and 20 robots will invade the city
    int[][][] newCity = invade(city, k);
    System.out.println("The invaded city.");    
    display(newCity);
    for (int i = 0; i<5; i++) { //update the city 5 times
      System.out.println("City update number " + (i+1));
      newCity = update(newCity);
      display(newCity);
    }
  }
  
  public static int[][][] update(int[][][] city) {
    int width = city.length;
    int height = city[0].length;   
    for (int j = width-1; j>=0; j--) {  //go through the columns
      for (int i = height-1; i>=0; i--) {  //go through the member arrays
        if(city[j][i][0] < 0 && i < (height-1)) { //find the negatives, not counting the rightmost row, go backwards to avoid followiong the negatives
          int g = city[j][i][0]; //make the negative entries positive
          g = -g;
          city[j][i][0] = g;
          int f;
          f = city[j][i+1][0]; //make the next entry to the right negative
          f = -f;
          city[j][i+1][0] = f;
        }
        else if (city[j][i][0] < 0 ) { //if a negative is in the rightmost row, just make it positive
          int g = city[j][i][0];
          g = -g;
          city[j][i][0] = g;          
        }
      }
    }
    return city;
  }
  
  public static int[][][] invade(int[][][] city, int k) {
    Random randomGenerator = new Random();      
    int width = city.length;
    int height = city[0].length;
    int[] kCordx = new int[k]; //these 2 arrays will be used to stop repeat coordinates from occuring
    int[] kCordy = new int[k];    
    int x,y;
    x = randomGenerator.nextInt(width-1); //get the first coordinates
    y = randomGenerator.nextInt(height-1);  
    kCordx[0] = x;
    kCordy[0] = y;  
    int f = city[x][y][0]; //make negative at those coordinates
    f = -f;
    city[x][y][0] = f;    
    for (int i = 1; i<k; i++) { //continue until k, all the robots, are used
      int q = 0;      
      while (q==0) { //after the first change, go back and make sure there are no repeats
        x = randomGenerator.nextInt(width-1);
        y = randomGenerator.nextInt(height-1);  
        kCordx[i] = x;
        kCordy[i] = y;        
        for (int j = 0; j < i; j++) {  //search for repeats with linear search          
          if (kCordx[j] == x && kCordy[j] == y) {
            q = 0;        
            break;
          }
          else {
            q = 1;    
          }
        }
        f = city[x][y][0];
        f = -f;
        city[x][y][0] = f;
      }
    }
    return city;
  }
  
  public static void display (int[][][] city) {
    for (int i = 0; i < city.length; i++) {
      for (int j = 0; j < city[0].length; j++) { //print each member array
        System.out.printf("%5S", city[i][j][0] + " "); //print f to ensure constant characters for a grid
      }
      System.out.println(); //start a new line for new member arrays
    }    
  }
  
  public static int[][][] buildCity() {
    Random randomGenerator = new Random();
    int colNum = 10 + randomGenerator.nextInt(5); //generate random nums from 10 to 15, this is how many member arrays there will be
    int rowNum = 10 + randomGenerator.nextInt(5); //generate the length of each number array (north-south block)
    int[][][] city = new int[colNum][rowNum][1];
    for (int i = 0; i < colNum; i++) {
      for (int j = 0; j < rowNum; j++) {
        city[i][j][0] = 100 + randomGenerator.nextInt(899); //pop of each block from 100 to 999
      }
    }
    return city;
  }
  
  
}