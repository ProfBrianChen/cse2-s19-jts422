//CSE2 hw10
//Jason Schanck
//Straight

import java.util.Random;

public class Straight {
  
  public static void main( String[] args) {
    double counter = 0;
    for (int i = 0; i < 1000000; i++) { //loop one million times
      int[] deck = deckForm();
      int[] fiveCards = draw(deck);
      int[] sortedCards = sort(fiveCards, 1);
      boolean straightTest = straight(sortedCards);
      if (straightTest == true) {
        counter++;
      }
    }
    double frequency = (counter/10000);
    System.out.println("There was a straight " + frequency + "% of the time.");
  }
  
  public static int[] sort(int[] cards, int k) {
    //sort the array
    for (int i = 0; i < 5; i++) {
      cards[i] = cards[i] % 13;
    }
    for (; k <= 5; k++) { //search for the k'th lowest term
      for (int i = k; i<=5; i++) {
        if(cards[i-1] < cards[k-1]) { //if a lower term is found, swap the values
          int temp = cards[i-1];
          cards[i-1] = cards[k-1];
          cards[k-1] = temp;
        }
      }
    }
    return cards;
  }
  
  public static boolean straight(int[] cards) {
    for (int j = 1; j < 5; j++) { //use j to see if the numbers increase by one, i.e. a straight
      if (cards[j] != cards[j-1] + 1) {
        return false;
      }
    }
    return true;
  }
  
  public static int[] draw(int[] deck) {
    int[] firstFive = new int[5]; //take the first five ints in the deck
    for (int i = 0; i < 5; i++) {
      firstFive[i] = deck[i];
    }
   return firstFive;
  }
  
  public static int[] deckForm() {
    Random ran = new Random();
    int[] deck = new int[52];
    for (int j = 0; j < 52; j++) {
      deck[j] = j;
    }
    for (int j = 0; j<52; j++) { //shuffle the deck
      int ranPos = ran.nextInt(51);
      int k = deck[j];
      deck[j] = deck[ranPos];
      deck[ranPos] = k;
    }
    return deck;
  }
  
  
}