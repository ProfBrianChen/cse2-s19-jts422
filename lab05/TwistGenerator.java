//CSE2 lab05
//Jason Schanck

//print out a simple twist on the screen

import java.util.Scanner;

public class TwistGenerator {
  
  public static void main (String [] args){
    Scanner myScanner = new Scanner( System.in );
    System.out.println("Enter a positive integer, length. ");
    boolean numCheck = myScanner.hasNextInt(); //check that the user entered an integer
    int length = 0; //declare a variable for length
    String junkWord = ""; //create a variable to store invalid inputs
    String line1 = ""; //declare strings for the 3 twist lines that will be printed
    String line2 = "";
    String line3 = "";
    while (numCheck == false){
        junkWord = myScanner.next(); //dispose of invalid inputs
        System.out.println("Enter a positive integer, length. "); //ask for input again
        numCheck = myScanner.hasNextInt();
          if (numCheck == true) {
            length = myScanner.nextInt(); //assign a new value to length
            if (length < 1) {
              numCheck = false; //check if the input is a positive integer
            }
            else {
              break;
            }
          }

    }
    if (numCheck == true) {
      if (length == 0) {
      length = myScanner.nextInt(); //assign the input to the variable length if it has not already been assigned in the last while statement
      }
      while (length > 0) {
        line1 = line1 + "\\"; //add to the lines the amount of times the number was entered
        line2 = line2 + " ";
        line3 = line3 + "/";
        length = length-1;
        if (length == 0) { //break as soon as the lines are complete
          break;
        }
        line1 = line1 + " ";
        line2 = line2 + "x";
        line3 = line3 + " ";
        length = length-1;
        if (length == 0) { //break as soon as the lines are complete
          break;
        }
        line1 = line1 + "/";
        line2 = line2 + " ";
        line3 = line3 + "\\";
        length = length-1;
      }
    }
    System.out.println(line1); //print out the twist
    System.out.println(line2);
    System.out.println(line3);
  }
}