//CSE2 lab9
//Jason Schanck

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class lab09 {
  public static void main ( String[] args) {
    Scanner scan = new Scanner( System.in );
    boolean check = false;
    int a = 0;
    while (check == false) {
      System.out.println("Would you like to perfrom a linear or binary search?"); //determine if the user wants a binary of linear search
      String input = scan.next();
      if (input.equals("linear")) {
        check = true; 
        a = 1;
      }
      else if (input.equals("binary")) {
        check = true; 
        a = 2;        
      }
      else {
        System.out.println("You must enter the word linear or binary.");
      }
    }
    
    System.out.println("Enter an array size."); //determine the size of the array the user wants
    int size = 0; //variable that user will enter
    boolean aCheck = scan.hasNextInt();
    String junkWord = "";
    if (aCheck == true) {
      size = scan.nextInt();
      if (size < 1) {
        aCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (aCheck == false) {
      if (size == 0) {
        junkWord = scan.next(); //dispose of invalid inputs
      }
      System.out.println("You need to input a positive integer!");
      System.out.println("Enter a positive integer for array size."); //ask for input again
      aCheck = scan.hasNextInt();
      if (aCheck == true) {
        size = scan.nextInt(); //assign a new value to num
        if (size < 1) {
          aCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
      else {
        junkWord = scan.next();
      }
    }

    System.out.println("Enter an integer search term."); //determine the term the user wants to search for
    int searchTerm = 0; //variable that user will enter
    boolean sCheck = scan.hasNextInt();
    if (sCheck == true) {
      searchTerm = scan.nextInt();
      if (searchTerm < 0) {
        sCheck = false; //check if the input is a positive integer between 1 and 10
       }
    }     
    while (sCheck == false) {
      if (searchTerm == 0) {
        junkWord = scan.next(); //dispose of invalid inputs
      }
      System.out.println("You need to input a positive integer!");
      System.out.println("Enter an integer search term."); //ask for input again
      sCheck = scan.hasNextInt();
      if (sCheck == true) {
        searchTerm = scan.nextInt(); //assign a new value to num
        if (searchTerm < 0) {
          sCheck = false; //check if the input is a positive integer between 1 and 10
        }
        else {
          break; //leave the while loop if the number is now valid
        }
      }
      else {
        junkWord = scan.next();
      }
    }
    int[] binArray = new int[size]; //create arrays for bin search and lin search
    int[] linArray = new int[size]; 
    System.out.println("The array: ");    
    if (a==1) { //if a = 1 the user wanted a lin search
       linArray = linArray(size); //call a method to create entries in the array
      for (int i = 0; i < size; i++) { //print the array
        System.out.println(linArray[i]);
      }
      int place = linSearch(linArray, searchTerm); //call a method to find the location of the integer
      System.out.println(searchTerm +  " is found at the following spot: " + place);  //print the location       
    }
    else if (a==2) { //if a = 2 the user wanted a bin search
      binArray = binArray(size);  
      for (int i = 0; i < size; i++) {
        System.out.println(binArray[i]);
      } 
      int place = binSearch(binArray, searchTerm);
      System.out.println(searchTerm +  " is found at the following spot: " + place);        
    }  
  }
  
  public static int[] linArray(int size) { //create entries for an array
    Random randomGenerator = new Random(); 
    int[] linArray = new int[size];
    for (int i = 0; i<size; i++) {
      linArray[i] = randomGenerator.nextInt(size);
    }
    return linArray;
  }
  public static int[] binArray(int size) { //create entriesd for an array
    Random randomGenerator = new Random(); 
    int[] binArray = new int[size];
    for (int i = 0; i < size; i++) {
      binArray[i] = randomGenerator.nextInt(size);
    }
    Arrays.sort(binArray); //sort the array for bin search
    return binArray;
  }
  
  public static int linSearch(int[] array, int searchTerm) { //conduct lin search
    int length = array.length;
    int place = -1;
    for (int i = 0; i< length; i++) {
      if (array[i] == searchTerm) {
        place = i;
        break;
      }
    }
    return place;
  }
  public static int binSearch(int[] array, int searchTerm) { //conduct bin search
    int length = array.length;
    int place = 0;
    int start = 1;
    while (place == 0) {
      if (length>=1) { //stop if the full array is searched through without finding the num
        int mid = start + ((length-1)/2); //find the middle of the array
        if (array[mid] == searchTerm) { //return the entry once the number is found
          return mid;
        }
        if (array[mid] > searchTerm) { //adjust the lower bound
          length = mid;
        }
        if (array[mid] < searchTerm) { //adjust the upper bound
          start = mid;
        }
      }
      else { //if the integer is not found, we want to return -1
        place = -1;
      }
    }
    return place;
  }
}